﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerM : MonoBehaviour
{
    public float velocidadY;
    public float maxY;
    private float direction;
    private Vector3 newPosition = new Vector3(0, 0, 0);
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float maxX;
    private float posX;


    void Update()
        
    {

        posX = transform.position.x + Input.GetAxis("Horizontal") * speed * Time.deltaTime;


        if (posX > maxX)
        {
            posX = maxX;
        }
        else if (posX < -maxX)
        {
            posX = -maxX;
        }

        transform.position = new Vector3(posX, transform.position.y, transform.position.z);
    }
}