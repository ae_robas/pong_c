﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadX;
    public float maxX;
    private float posX;
    private float direction;

    // Update is called once per frame
    void Update()
    {

        direction = Input.GetAxis("Horizontal");

        posX = transform.position.y + direction * velocidadX * Time.deltaTime;

        if (posX > maxX)
        {
            posX = maxX;
        }
        else if (posX < -maxX)
        {
            posX = -maxX;
        }

        transform.position = new Vector3(transform.position.y, posX, transform.position.z);
    }
}